#include <openvf/openvf.h>
#include <stdio.h>
#include <stdlib.h>

const char *type_names[] = {
    "VF_BYTE",
    "VF_UNSIGNED_BYTE",
    "VF_SHORT",
    "VF_UNSIGNED_SHORT",
    "VF_INT",
    "VF_UNSIGNED_INT",
    "VF_FLOAT",
    "VF_LONG",
    "VF_UNSIGNED_LONG",
    "VF_DOUBLE",
    "VF_INT_2_10_10_10_REV",
    "VF_INT_10_10_10_2"
};

unsigned int get_stride(uint32_t datatype) {
    switch (datatype) {
        case VF_BYTE:
        case VF_UNSIGNED_BYTE:
            return 1;
        case VF_SHORT:
        case VF_UNSIGNED_SHORT:
            return 2;
        case VF_INT:
        case VF_UNSIGNED_INT:
        case VF_FLOAT:
        case VF_INT_2_10_10_10_REV:
        case VF_INT_10_10_10_2:
            return 4;
        case VF_LONG:
        case VF_UNSIGNED_LONG:
        case VF_DOUBLE:
            return 8;
        default:
            return 0;
    }
}

void print_component (const char *data, uint32_t type) {
    switch (type) {
        case VF_BYTE:
            printf("%d", (int)(*(const int8_t*)data));
            break;
        case VF_UNSIGNED_BYTE:
            printf("%u", (unsigned int)(*(const uint8_t*)data));
            break;
        case VF_SHORT:
            printf("%d", (int)(*(const int16_t*)data));
            break;
        case VF_UNSIGNED_SHORT:
            printf("%u", (unsigned int)(*(const uint16_t*)data));
            break;
        case VF_INT:
            printf("%d", *(const int32_t*)data);
            break;
        case VF_UNSIGNED_INT:
            printf("%d", *(const uint32_t*)data);
            break;
        case VF_FLOAT:
            printf("%f", *(const float*)data);
            break;
        case VF_INT_2_10_10_10_REV:
        case VF_INT_10_10_10_2:
            // TODO
            break;
        case VF_LONG:
            printf("%l", *(const int64_t*)data);
            break;
        case VF_UNSIGNED_LONG:
            printf("%lu", *(const uint64_t*)data);
            break;
        case VF_DOUBLE:
            printf("%lf", *(const double*)data);
            break;
        default:
            break;
    }

}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s [filename]\n", argv[0]);
        return EXIT_FAILURE;
    }

    vf_t *vf = vfAlloc();
    if (vf == NULL) {
        fprintf(stderr, "vfAlloc() failed\n");
        return EXIT_FAILURE;
    }
    vf_error_t err;
    if (err = vfLoad(vf, argv[1])) {
        vfFree(vf);
        fprintf(stderr, "vfLoad() failed: %d", err);
        return EXIT_FAILURE;
    }

    for (vf_set_t *set = vfGetFirstSet(vf); set != NULL; set = vfGetNextSet(set)) {
        printf("%s (%d %s) [%d]\n", vfGetIdentifier(set), vfGetComponents(set), type_names[vfGetDataType(set)], vfGetLength(set));
        const char *data = (const char*) vfGetData(set);
        unsigned int stride = get_stride(vfGetDataType(set));

        for (uint32_t i = 0; i < vfGetLength(set); i++) {
            printf("  { ");
            for (uint32_t j = 0; j < vfGetComponents(set); j++) {
                print_component(data + (i * vfGetComponents(set) + j) * stride, vfGetDataType(set));
                if (j != vfGetComponents(set) - 1) {
                    printf(", ");
                }
            }
            printf(" }\n");
        }
    }

    vfFree(vf);

}