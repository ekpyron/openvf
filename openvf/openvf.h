/*
 * This file is part of openvf.
 *
 * openvf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * openvf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with openvf.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENVF_H
#define OPENVF_H

#include <stdint.h>

#define VFCALL
#define VFEXPORT

#ifdef __cplusplus
extern "C" {
#endif

typedef struct vf vf_t;
typedef struct vf_set vf_set_t;
typedef uint64_t (VFCALL *vf_readfn_t) (void *buf, uint64_t length, void *userdata);
typedef uint64_t (VFCALL *vf_writefn_t) (void *buf, uint64_t length, void *userdata);

#define VF_BYTE                                 0x00000000
#define VF_UNSIGNED_BYTE                        0x00000001
#define VF_SHORT                                0x00000002
#define VF_UNSIGNED_SHORT                       0x00000003
#define VF_INT                                  0x00000004
#define VF_UNSIGNED_INT                         0x00000005
#define VF_FLOAT                                0x00000006
#define VF_LONG                                 0x00000007
#define VF_UNSIGNED_LONG                        0x00000008
#define VF_DOUBLE                               0x00000009
#define VF_INT_2_10_10_10_REV                   0x0000000A
#define VF_INT_10_10_10_2                       0x0000000B

#define VF_NO_ERROR			0
#define VF_ERROR_IO			1
#define VF_ERROR_INVALID_MAGIC		2
#define VF_ERROR_OUT_OF_MEMORY		3
#define VF_ERROR_INVALID_DATA		4
#define VF_ERROR_INVALID_ARGUMENTS	5
#define VF_ERROR_UNSUPPORTED		6

#define VF_FLAGS_LZ4		0x0001
#define VF_FLAGS_RESERVED	0xFFFE


typedef unsigned int vf_error_t;

VFEXPORT vf_t *vfAlloc (void);
VFEXPORT void VFCALL vfFree (vf_t *vf);

VFEXPORT vf_error_t VFCALL vfLoad (vf_t *vf, const char *filename);
VFEXPORT vf_error_t VFCALL vfLoadCustom (vf_t *vf, vf_readfn_t readfn, void *userdata);

VFEXPORT uint32_t VFCALL vfDataTypeSize (uint32_t datatype);

VFEXPORT vf_set_t *VFCALL vfGetSet (vf_t *vf, const char *identifier);
VFEXPORT vf_set_t *VFCALL vfGetFirstSet (vf_t *vf);
VFEXPORT vf_set_t *VFCALL vfGetNextSet (vf_set_t *set);

VFEXPORT uint32_t VFCALL vfGetLength (vf_set_t *set);
VFEXPORT uint32_t VFCALL vfGetDataType (vf_set_t *set);
VFEXPORT uint32_t VFCALL vfGetComponents (vf_set_t *set);
VFEXPORT const char *VFCALL vfGetIdentifier (vf_set_t *set);
VFEXPORT void *VFCALL vfGetData (vf_set_t *set);

VFEXPORT vf_error_t VFCALL vfAddSet (vf_t *vf, const char *name, uint16_t components, uint16_t datatype,
		uint64_t length, const void *data, uint16_t flags);

VFEXPORT vf_error_t VFCALL vfSave (vf_t *vf, const char *filename);
VFEXPORT vf_error_t VFCALL vfSaveCustom (vf_t *vf, vf_writefn_t writefn, void *userdata);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* !defined OPENVF_H */
