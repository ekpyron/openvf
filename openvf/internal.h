/*
 * This file is part of openvf.
 *
 * openvf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * openvf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with openvf.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENVF_INTERNAL_H
#define OPENVF_INTERNAL_H

#include "openvf.h"

struct _vf_set_header
{
	uint16_t components;
	uint16_t datatype;
	uint16_t idlen;
	uint16_t flags;
	uint64_t length;
};

struct vf_set
{
	struct _vf_set_header header;
	void *data;
	char *identifier;
	struct vf_set *next;
};

struct vf
{
	uint8_t magic[8];
	struct vf_set *first;
};

#endif /* !defined OPENVF_INTERNAL_H */
