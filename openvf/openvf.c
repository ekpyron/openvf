/*
 * This file is part of openvf.
 *
 * openvf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * openvf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with openvf.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "internal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef ENABLE_LZ4
#include "lz4/lz4.h"
#include "lz4/lz4hc.h"
#endif

VFEXPORT vf_t *VFCALL vfAlloc (void)
{
	vf_t *vf;
	vf = (vf_t*) malloc (sizeof (vf_t));
	vf->first = NULL;
	return vf;
}

uint64_t VFCALL _vfReadStdio (void *buf, uint64_t length, void *userdata)
{
	return fread (buf, 1, (size_t)length, (FILE*) userdata);
}

uint64_t VFCALL _vfWriteStdio (void *buf, uint64_t length, void *userdata)
{
	return fwrite (buf, 1, (size_t)length, (FILE*) userdata);
}

VFEXPORT vf_error_t VFCALL vfLoad (vf_t *vf, const char *name)
{
	FILE *file = fopen (name, "rb");
	if (file == NULL)
		return VF_ERROR_IO;
	return vfLoadCustom (vf, _vfReadStdio, file);
}

VFEXPORT uint32_t VFCALL vfDataTypeSize (uint32_t datatype)
{
	switch (datatype)
	{
	case VF_BYTE:
	case VF_UNSIGNED_BYTE:
		return 1;
	case VF_SHORT:
	case VF_UNSIGNED_SHORT:
		return 2;
	case VF_INT:
	case VF_INT_2_10_10_10_REV:
	case VF_INT_10_10_10_2:
	case VF_UNSIGNED_INT:
	case VF_FLOAT:
		return 4;
	case VF_LONG:
	case VF_UNSIGNED_LONG:
	case VF_DOUBLE:
		return 8;
	}
	return 0;
}

vf_error_t _vfWriteSet (vf_set_t *set, vf_writefn_t writefn, void *userdata)
{
	uint64_t length;

	if (writefn (&set->header, sizeof (struct _vf_set_header), userdata)
			!= sizeof (struct _vf_set_header))
	{
		return VF_ERROR_IO;
	}
	if (writefn (&set->identifier[0],set->header.idlen, userdata)
			!= set->header.idlen)
	{
		return VF_ERROR_IO;
	}
	length = vfDataTypeSize (set->header.datatype) * set->header.components;
	length *= set->header.length;

	if (writefn (set->data, length, userdata) != length)
		return VF_ERROR_IO;

	return VF_NO_ERROR;
}

vf_error_t _vfWriteSetLZ4 (vf_set_t *set, vf_writefn_t writefn, void *userdata)
{
#ifdef ENABLE_LZ4
	uint64_t length, complen;
	void *compdata;

	if (writefn (&set->header, sizeof (struct _vf_set_header), userdata)
			!= sizeof (struct _vf_set_header))
	{
		return VF_ERROR_IO;
	}
	if (writefn (&set->identifier[0],set->header.idlen, userdata)
			!= set->header.idlen)
	{
		return VF_ERROR_IO;
	}
	length = vfDataTypeSize (set->header.datatype) * set->header.components;
	length *= set->header.length;

	compdata = malloc (LZ4_compressBound (length));
	if (compdata == NULL)
		return VF_ERROR_OUT_OF_MEMORY;

	complen = LZ4_compressHC2 (set->data, compdata, length, 16);

	if (writefn (&complen, sizeof (uint64_t), userdata) != sizeof (uint64_t))
	{
		free (compdata);
		return VF_ERROR_IO;
	}

	if (writefn (compdata, complen, userdata) != complen)
	{
		free (compdata);
		return VF_ERROR_IO;
	}

	free (compdata);

	return VF_NO_ERROR;
#else
	return VF_ERROR_UNSUPPORTED;
#endif
}


VFEXPORT vf_error_t VFCALL _vfReadSet (vf_set_t *set, vf_readfn_t readfn, void *userdata)
{
	uint64_t length;

	if (set->header.flags & VF_FLAGS_RESERVED)
		return VF_ERROR_INVALID_DATA;

	set->identifier = malloc (set->header.idlen + 1);
	if (set->identifier == NULL)
		return VF_ERROR_OUT_OF_MEMORY;

	if (readfn (set->identifier, set->header.idlen, userdata) != set->header.idlen)
		return VF_ERROR_IO;
	set->identifier[set->header.idlen] = 0;

	length = vfDataTypeSize (set->header.datatype) * set->header.components;
	length *= set->header.length;
	set->data = malloc ((size_t)length);
	if (set->data == NULL)
		return VF_ERROR_OUT_OF_MEMORY;

	if (set->header.flags & VF_FLAGS_LZ4)
	{
#ifdef ENABLE_LZ4
		uint64_t complen;
		void *compdata;

		if (readfn (&complen, sizeof (uint64_t), userdata) != sizeof (uint64_t))
			return VF_ERROR_IO;

		compdata = malloc (complen);
		if (compdata == NULL)
			return VF_ERROR_OUT_OF_MEMORY;

		if (readfn (compdata, complen, userdata) != complen)
		{
			free (compdata);
			return VF_ERROR_IO;
		}

		if (LZ4_decompress_fast (compdata, set->data, length) < 0)
		{
			free (compdata);
			return VF_ERROR_INVALID_DATA;
		}

		free (compdata);
#else
		return VF_ERROR_UNSUPPORTED;
#endif
	}
	else if (readfn (set->data, length, userdata) != length)
		return VF_ERROR_IO;

	return VF_NO_ERROR;
}

VFEXPORT vf_error_t VFCALL vfLoadCustom (vf_t *vf, vf_readfn_t readfn, void *userdata)
{
	if (readfn (&vf->magic[0], 8, userdata) != 8)
		return VF_ERROR_IO;

	static uint8_t magic[8] = { 0x7F, 'O', 'P', 'E', 'N', 'V', 'F', 0x00 };

	if (memcmp (&magic[0], &vf->magic[0], 8))
		return VF_ERROR_INVALID_MAGIC;

	vf_set_t *set = (vf_set_t*) malloc (sizeof (vf_set_t));
	if (set == NULL)
		return VF_ERROR_OUT_OF_MEMORY;

	while (readfn (&set->header, sizeof (struct _vf_set_header), userdata) == sizeof (struct _vf_set_header))
	{
		set->data = NULL;
		set->identifier = NULL;
		vf_error_t err = _vfReadSet (set, readfn, userdata);
		if (err != VF_NO_ERROR)
		{
			if (set->data != NULL) free (set->data);
			if (set->identifier != NULL) free (set->identifier);
			free (set);
			return err;
		}
		set->next = vf->first;
		vf->first = set;
		set = (vf_set_t*) malloc (sizeof (vf_set_t));
		if (set == NULL)
			return VF_ERROR_OUT_OF_MEMORY;
	}
	free (set);

	return VF_NO_ERROR;
}

VFEXPORT void VFCALL vfFree (vf_t *vf)
{
	while (vf->first != NULL)
	{
		vf_set_t *set = vf->first;
		vf->first = set->next;
		if (set->identifier != NULL)
			free (set->identifier);
		if (set->data != NULL)
			free (set->data);
		free (set);
	}
	free (vf);
}

VFEXPORT const char *VFCALL vfGetIdentifier (vf_set_t *set)
{
	return set->identifier;
}

VFEXPORT vf_set_t *VFCALL vfGetSet (vf_t *vf, const char *identifier)
{
	vf_set_t *set;
	size_t length = strlen (identifier);
	for (set = vf->first; set != NULL; set = set->next)
	{
		if (length == set->header.idlen
			&& !strncmp (identifier, set->identifier, length))
			return set;
	}
	return NULL;
}

VFEXPORT vf_set_t *VFCALL vfGetFirstSet (vf_t *vf)
{
	return vf->first;
}

VFEXPORT vf_set_t *VFCALL vfGetNextSet (vf_set_t *set)
{
	return set->next;
}

VFEXPORT uint32_t VFCALL vfGetLength (vf_set_t *set)
{
	return (uint32_t)set->header.length;
}

VFEXPORT uint32_t VFCALL vfGetDataType (vf_set_t *set)
{
	return set->header.datatype;
}

VFEXPORT uint32_t VFCALL vfGetComponents (vf_set_t *set)
{
	return set->header.components;
}

VFEXPORT void *VFCALL vfGetData (vf_set_t *set)
{
	return set->data;
}

VFEXPORT vf_error_t VFCALL vfAddSet (vf_t *vf, const char *identifier, uint16_t components, uint16_t datatype,
		uint64_t length, const void *data, uint16_t flags)
{
	if (flags & VF_FLAGS_RESERVED)
		return VF_ERROR_INVALID_ARGUMENTS;
	vf_set_t *set = (vf_set_t*) malloc (sizeof (vf_set_t));
	if (set == NULL)
		return VF_ERROR_OUT_OF_MEMORY;
	set->header.idlen = strlen (identifier);
	set->header.components = components;
	set->header.datatype = datatype;
	set->header.length = length;
	set->header.flags = flags;

	set->identifier = (char*) malloc (set->header.idlen + 1);
	if (set->identifier == NULL)
	{
		free (set);
		return VF_ERROR_OUT_OF_MEMORY;
	}
	strncpy (&set->identifier[0], identifier, set->header.idlen);
	set->identifier[set->header.idlen] = 0;

	uint64_t len;
	len = vfDataTypeSize (set->header.datatype) * set->header.components;
	len *= set->header.length;
	set->data = malloc ((size_t)len);
	if (set->data == NULL)
	{
		free (set->identifier);
		free (set);
		return VF_ERROR_OUT_OF_MEMORY;
	}
	memcpy (set->data, data, len);
	set->next = vf->first;
	vf->first = set;
	return VF_NO_ERROR;
}

VFEXPORT vf_error_t VFCALL vfSave (vf_t *vf, const char *name)
{
	FILE *file = fopen (name, "wb");
	if (file == NULL)
		return VF_ERROR_IO;
	return vfSaveCustom (vf, _vfWriteStdio, file);
}

VFEXPORT vf_error_t VFCALL vfSaveCustom (vf_t *vf, vf_writefn_t writefn, void *userdata)
{
	static uint8_t magic[8] = { 0x7F, 'O', 'P', 'E', 'N', 'V', 'F', 0x00 };
	if (writefn (&magic[0], 8, userdata) != 8)
		return VF_ERROR_IO;

	vf_set_t *set;
	for (set = vf->first; set != NULL; set = set->next)
	{
		vf_error_t err;
		if (set->header.flags & VF_FLAGS_LZ4)
			err = _vfWriteSetLZ4 (set, writefn, userdata);
		else
			err = _vfWriteSet (set, writefn, userdata);
		if (err != VF_NO_ERROR)
			return err;
	}

	return VF_NO_ERROR;
}
