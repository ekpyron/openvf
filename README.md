OPENVF README
=============

Overview
--------

OpenVF consists of a library with no external dependencies and two conversion utilities _any2vf_ (which converts only
single meshes) and _dae2vf_ (which can parse and convert _COLLADA_ files and automatically extract animations and node
hierarchies).

Dependencies
------------

- cmake (http://www.cmake.org/) is used as build system.
- any2vf depends on asset importer (http://assimp.sourceforge.net/)
- dae2vf depends on OpenCOLLADA (https://github.com/khronosGroup/OpenCOLLADA/) and yaml-cpp (https://code.google.com/p/yaml-cpp/)
